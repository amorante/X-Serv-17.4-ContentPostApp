#!/usr/bin/python3

import webapp


class contentApp(webapp.webApp):
    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        method = request.split(' ', 2)[0]
        resource = request.split(' ', 2)[1]
        body = request.split('\n')[-1]
        return method, resource, body

    def process(self, petition):

        method, resource, body = petition

        form = """
            <br/><br/>
            <form action="" method = "POST"> 
            <input type="text" name="name" value="">
            <input type="submit" value="Send">
            </form>
        """

        if method == "GET":
            if resource in self.content.keys():
                httpCode = "200 OK"
                htmlBody = "<html><body>Found! You have requested: " + resource + "<br>" \
                           + "Content: " + self.content[resource] + \
                           "<p>Use this form if you want to add a new content.</p>" + form + "</body></html>"

            else:
                httpCode = "404 Not Found"
                htmlBody = "<html><body>Not Found. You can add a new content." + form + "</body></html>"
            return httpCode, htmlBody

        else:
            self.content[resource] = body.split('=')[1]
            httpCode = '200 OK'
            htmlBody = "<html><body>You have requested: " + resource + "<br>" \
                       + "Content: " + self.content[resource] + \
                       "<p>Use this form if you want to add a new content.</p>" + form + "</body></html>"

        return httpCode, htmlBody


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
